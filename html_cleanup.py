import re

def remove_pair_html_tags(wiki_markup):
    i = wiki_markup.find('<')
    if wiki_markup[i+1:].lstrip()[0] != '/':
        tag = re.split(r'\W', wiki_markup[i+1:].lstrip())[0]
        wiki_markup = wiki_markup[0:i] + re.sub(r'^.*?<\s*/\s*' + tag + r'[^>]*>', '', wiki_markup[i:])
    return wiki_markup

def remove_single_html_tags(wiki_markup):
    wiki_markup = re.sub(r'<\s*br\s*/?>', ' ', wiki_markup)
    return re.sub(r'<[^<>]*>', '', wiki_markup)

# remove single html-tags and wiki markup {{}}, [[]], and text in braces ()
# not all but the most inner ones
def remove_tags(wiki_markup):
    wiki_markup = remove_single_html_tags(wiki_markup)
    wiki_markup = re.sub(r'\([^())]*\)', '', wiki_markup)
    wiki_markup = re.sub(r'\{[^{}}]*\}', '', wiki_markup)
    wiki_markup = re.sub(r'\[[^\[\]]*\]', '', wiki_markup)
    return wiki_markup;

def iterated_remove_tags(wiki_markup):
    # remove all pair tags
    wiki_markup_new = remove_pair_html_tags(wiki_markup)
    while wiki_markup_new != wiki_markup:
        wiki_markup = wiki_markup_new
        wiki_markup_new = remove_pair_html_tags(wiki_markup)

    # remove all single html-tags and wiki markup {{}}, [[]], and text in braces ()
    wiki_markup_new = remove_tags(wiki_markup)
    while wiki_markup_new != wiki_markup:
        wiki_markup = wiki_markup_new
        wiki_markup_new = remove_tags(wiki_markup)
    return wiki_markup
