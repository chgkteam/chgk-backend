import urllib.parse
import urllib.request
import urllib.error
import json
import re
import datetime
import sys
import time # for time.sleep
from caching import *
from caching_to_db import *
from invocation_logging import *

API_BASE   = 'https://en.wikipedia.org/w/api.php'
WIKIDATA_API_BASE = 'https://www.wikidata.org/w/api.php'
INDEX_BASE = 'https://en.wikipedia.org/w/index.php'

# caching by URL is much more efficient when params are in the same order each time
def urlencoded_fix_order(params):
    return urllib.parse.urlencode(sorted(params.items()), doseq=True)

def stable_url(urlBase, params):
    return urlBase + '?' + urlencoded_fix_order(params)

@loggingDecorator(file=sys.stderr, logInvocations=False, logResults=False, logFailures=True)
@cacheRequestDecorator(*dbCachingPairByURI(lambda url: url))
def contentByURL(url):
    return urllib.request.urlopen(url).read().decode('utf-8')

################

def summary_content_url(pageName):
    params = {
        'format': 'json',
        'action': 'query',
        'prop': 'extracts',
        'exintro': '',
        'explaintext': '',
        'titles': pageName,
    }
    return stable_url(API_BASE, params)

@cacheRequestDecorator(*dbCachingPairByURI(summary_content_url))
def getSummary(pageName):
    content = contentByURL(summary_content_url(pageName))
    return list(json.loads(content)['query']['pages'].values())[0]['extract']


# https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&ppprop=wikibase_item&titles=Battle_of_Kulikovo&redirects=1&formatversion=2
def getWikidataID_url(pageName):
    params = {
        'format': 'json',
        'action': 'query',
        'prop': 'pageprops',
        'ppprop': 'wikibase_item',
        'titles': pageName,
        'redirects': 1,
        'formatversion': 2,
    }
    return stable_url(API_BASE, params)

def getWikidataID(pageName):
    content = contentByURL(getWikidataID_url(pageName))
    return json.loads(content)['query']['pages'][0]['pageprops']['wikibase_item']

#https://www.wikidata.org/w/api.php?action=wbgetclaims&entity=Q180736&languages=en&formatversion=2
def getWikidataProperties_url(wikidataID):
    params = {
        'format': 'json',
        'action': 'wbgetclaims',
        'entity': wikidataID,
        'formatversion': 2,
    }
    return stable_url(WIKIDATA_API_BASE, params)

def getWikidataProperties(wikidataID):
    content = contentByURL(getWikidataProperties_url(wikidataID))
    return json.loads(content)

def wikidataPropertiesByTitle(pageName):
    return getWikidataProperties(getWikidataID(pageName))

################

def getPage(pageName, followRedirects=True):
    if followRedirects:
        return getPageFollowingRedirects(pageName)
    else:
        return getPageNoFollowingRedirects(pageName)

def page_raw_content_url(pageName):
    params = {
        'title': pageName,
        'action': 'raw'
    }
    return stable_url(INDEX_BASE, params)

@cacheRequestDecorator(*dbCachingPairByURI(page_raw_content_url))
def getPageNoFollowingRedirects(pageName):
    return contentByURL(page_raw_content_url(pageName))

@cacheRequestDecorator(*dbCachingPairByURI(lambda pageName: page_raw_content_url(pageName) + '->followRedirect'))
def getPageFollowingRedirects(pageName):
    content = getPageNoFollowingRedirects(pageName)
    if content.startswith('#REDIRECT '):
        realName = re.search(r'#REDIRECT\s\[\[(.*?)\]\]', content)
        realName = realName.groups()[0]
        return getPageFollowingRedirects(realName)
    else:
        return content


################

# @cacheRequestDecorator(cacheFileLoader(categoryPagesCacheFilename), cacheFileStorer(categoryPagesCacheFilename))
def categoryPages(categoryName, cmtype='page', cmcontinue=None):
    params = {
        'action': 'query',
        'list': 'categorymembers',
        'format': 'json',
        'cmlimit': '500',
        'cmtitle': categoryName,
        'cmtype': cmtype,
    }
    if cmcontinue:
        params['cmcontinue'] = cmcontinue
    return contentByURL(stable_url(API_BASE, params))

################

def transcludedInLimited(templateName, gticontinue='', gtilimit='max'):
    params = {
        'action': 'query',
        'generator': 'transcludedin',
        'titles': templateName,
        'format': 'json',
        'gtilimit': gtilimit,
        'gtinamespace': 0,
        'gticontinue': gticontinue
    }
    return contentByURL(stable_url(API_BASE, params))


def transcludedIn(templateName, gtilimit='max'):
    res = json.loads( transcludedInLimited(templateName, gtilimit=gtilimit) )
    for page in res['query']['pages'].values():
        yield page

    while 'continue' in res:
        time.sleep(5)
        res = json.loads( transcludedInLimited(templateName, gticontinue=res['continue']['gticontinue'], gtilimit=gtilimit) )
        for page in res['query']['pages'].values():
            yield page

################
def pageview_url(pageName, granularity='monthly', fromDate='20161201', toDate='20170101'):
    # ToDo: change date to present
    url_start = 'https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia.org/desktop/all-agents/'
    today = '/{}'.format(toDate)
    last_day = '/{}'.format(fromDate)
    often = '/{}'.format(granularity)
    name = pageName.replace(' ', '_')
    url = url_start + urllib.parse.quote(name, safe='') + often + last_day + today
    return url

@loggingDecorator(file=sys.stderr, logInvocations=False, logResults=False, logFailures=True)
@cacheRequestDecorator(*dbCachingPairByURI(pageview_url))
def getViewsPage(pageName, granularity='monthly', fromDate='20161201', toDate='20170101'):
    return contentByURL(pageview_url(pageName, granularity=granularity, fromDate=fromDate, toDate=toDate))

def pageCoordinates_url(name):
    params = {
        'action': 'query',
        'prop': 'coordinates',
        'titles': name,
        'format': 'json',
        'coprop': 'dim'
    }
    return stable_url(API_BASE, params)

def pageCoordinates(name, placeInfo = None):
    coordsAns = contentByURL(pageCoordinates_url(name))
    coordsAns = json.loads(coordsAns)

    try:
        coordinate_properties = list(coordsAns['query']['pages'].values())[0]['coordinates'][0]
        return {
            'lat': coordinate_properties['lat'],
            'lng': coordinate_properties['lon'],
            'rad': int(coordinate_properties['dim'])  if ('dim' in coordinate_properties)  else None,
        }

    except Exception as e:
        return None
