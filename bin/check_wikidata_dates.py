# -*- coding: utf8 -*-

import os, sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import apiRequest
import wikidata_dates
import parsing_dates
import datetime

pageName1 = 'Battle_of_Stalingrad'#'Battle_of_Kulikovo'# input().strip()
pageName2 = 'Battle_of_Kulikovo'
date1 = wikidata_dates.getWikidataDate(apiRequest.getWikidataProperties(apiRequest.getWikidataID(pageName1)))
date2 = wikidata_dates.getWikidataDate(apiRequest.getWikidataProperties(apiRequest.getWikidataID(pageName2)))
#list_properties = apiRequest.getListWikidataProperties(apiRequest.getWikidataProperties(apiRequest.getWikidataID(pageName)), 'claims')
#print(apiRequest.getWikidataProperties(apiRequest.getWikidataID(pageName)))
with sys.stdout as fw:
	#print (list_properties, file = fw)
	print(pageName1, date1, file = fw)
	print(pageName2, date2, file = fw)
