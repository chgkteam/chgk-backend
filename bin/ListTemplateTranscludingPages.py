import sys
import os.path

currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import apiRequest

def main():
    templateName = 'Template:' + sys.argv[1]
    for page in apiRequest.transcludedIn(templateName):
        print(page['title'])

if __name__ == '__main__':
    main()
