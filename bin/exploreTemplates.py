import os
import sys
import traceback

currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import wikilib
import wiki_template
import apiRequest


def normalizeTemplateName(pageName):
    # First letter in page name doesn't matter
    # See for more details https://en.wikipedia.org/wiki/Wikipedia:Naming_conventions_(technical_restrictions)
    # ToDo:
    #   First letter of page title doesn't matter, but when namespace's provided, we should first locate first letter of title.
    #   So `Template:Infobox military conflict` and `Template:infobox military conflict` should be treated equal.
    #   But it's better to first read manual on naming conventions as this is just an observation, not a knowledge
    titleCase = str.upper(pageName[0]) + pageName[1:]
    return titleCase.replace(' ', '_')

def parseInfoboxes(page, options):    
    templates = wiki_template.get_parsed_templates_on_page(page)
    relevantTemplates = filter(lambda template: normalizeTemplateName(template['template_name']) == normalizeTemplateName(options.infoboxName), templates)
    for template in relevantTemplates:
        if options.infoboxProperties:
            for infoboxProperty in options.infoboxProperties:
                output_row = [pageName, infoboxProperty, template['options'].get(infoboxProperty, '--MISSING-FIELD--')]
                print('\t'.join(output_row))
        else:
            print('-' * 80)
            print('> ' + pageName)
            print(template['raw_text'])
            print('-' * 80)

def processPage(pageName, options):
    try:
        page = apiRequest.getPage(pageName)
        parseInfoboxes(page, options)
    except Exception as e:
        print('-' * 80, file = sys.stderr)
        print('Failed to get or to parse infobox on page `' + pageName + '`.', file = sys.stderr)
        traceback.print_exc(file = sys.stderr)
        print('-' * 80, file = sys.stderr)

import argparse
optionParser = argparse.ArgumentParser(description='Print value of some property from infoboxes (or other transcluded templates) on page')
optionParser.add_argument('infoboxName', help='Name of infobox to obtain from pages')
optionParser.add_argument('--properties', help='List of properties you want to look at', dest='infoboxProperties', nargs='+')
optionParser.add_argument('--pages', help='List of pages you want to parse', dest='wikiPages', nargs='+')

optionParser.add_argument('--lang', help='Wikipedia language (default is English Wikipedia)', dest='wikipedia_language', default='en')

# # More universal approach
# optionParser.add_argument('--api-endpoint', help='Wikimedia API endpoint (default is English Wikipedia)', dest='api_endpoint', default='https://en.wikipedia.org/w/api.php')

options = optionParser.parse_args() # same as `optionParser.parse_args(sys.argv[1:])`

# Dirty hack
apiRequest.API_BASE   = 'https://' + options.wikipedia_language + '.wikipedia.org/w/api.php'
apiRequest.INDEX_BASE = 'https://' + options.wikipedia_language + '.wikipedia.org/w/index.php'

if options.wikiPages == None:
    for line in sys.stdin:
        pageName = line.strip()
        processPage(pageName, options)
else:
    for pageName in options.wikiPages:
        processPage(pageName, options)
