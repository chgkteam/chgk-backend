import os, sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from wikilib import *
from apiRequest import getPage

if __name__ == '__main__':
    """input('Введите название страницы:\n'"""
    page = get_page('en', 'Battle_of_Lake_Khasan')
    js = json.loads(page.decode('UTF-8'), encoding = 'UTF-8')
    content = get_infobox(get_first(js['query']['pages'])['revisions'][0]['*'])
    with open('out.log', 'w', encoding='utf8') as fw:
        print(wiki_template.parse_template(content)['options'], file=fw)
        print(parse_infobox_military_conflict(content), file=fw)
    #print('_=###########################')
    #print(get_infobox(content))
    #print('#################')
    #print(has_infobox(content, 'military conflict'))
    #page = getPage('usa', followRedirects=False)
    #print(page)
    #print(parse_infobox_scientist(get_infobox(page)))
