import dateutil.parser
import re
import sys, traceback

# P580 - start time
# P582 - end time
# P585 - point in time

# List of properties
def getListWikidataProperties(wikidataContents, head_property):
    list_properties_in_head_property = wikidataContents[head_property].keys()
    return list_properties_in_head_property

def filterClaims(wikidataContents, wikidata_property):
    if wikidata_property in wikidataContents['claims']:
        return wikidataContents['claims'][wikidata_property]
    else:
        return []

def extractTimeFromClaim(claim):
    return claim['mainsnak']['datavalue']['value']['time']

# "+1380-09-08T00:00:00Z"
def wikidata_to_date_with_era(date_string, wikidata_format: True):
    try:
        match = re.match(r'^(?P<sign>[+-])(?P<year>\d{4,})-(?P<month>\d\d)-(?P<day>\d\d)T(?P<hour>\d\d):(?P<minute>\d\d):(?P<second>\d\d(\.\d*)?)(?P<timezone>Z)', date_string)
        infos = match.groupdict()
        year, month, day = int(infos['year'],base=10), int(infos['month'],base=10), int(infos['day'],base=10)
        if day == 0:
            day = None
        if month == 0:
            month = None

        if infos['sign'] == '-':
            era = 'bc'
            if not wikidata_format:
                year = year + 1 # (in ISO-8601) year=+0000 is 1 BC; (in wikidata JSON-format) 1 BC is -0001
        else:
            era = 'ad'
        return [day, month, year, era]
    except:
        print('wikidata_to_date_with_era', date_string, file=sys.stderr)
        traceback.print_exc(file=sys.stderr)
        raise

def wikidata_to_date(date_string):
    if date_string[0] == '-':  # dateutils can't read sign of the year
        i = -1
    else:
        i = 1
    parsed_date = dateutil.parser.parse(date_string[1:])
    return [parsed_date.day, parsed_date.month, parsed_date.year * i]

def transformationWikidataDate(wikidataContents, wikidata_property):
    claims = filterClaims(wikidataContents, wikidata_property)
    date_string = extractTimeFromClaim(claims[0])
    return wikidata_to_date(date_string)

def getWikidataDate(wikidataContents, wikidata_property='P585'):
    try:
        date_1 = date_2 = [None, None, None]
        list_properties_in_claims = getListWikidataProperties(wikidataContents, 'claims')
        wikidata_property = 'P580'
        if wikidata_property in list_properties_in_claims:
            date_1 = transformationWikidataDate(wikidataContents, wikidata_property)
            wikidata_property = 'P582'
            if wikidata_property in list_properties_in_claims:
                date_2 = transformationWikidataDate(wikidataContents, wikidata_property)
        else:
            wikidata_property = 'P585'
            if wikidata_property in list_properties_in_claims:
                date_1 = date_2 = transformationWikidataDate(wikidataContents, wikidata_property)

        return date_1, date_2
    except Exception:
        return None

def padWithZeros(number, minLen):
    if len(str(number)) >= minLen:
        return str(number)
    else:
        return '0' * (minLen - len(str(number))) + str(number)

def dateToWikidataFormat(day, month, year, era, withPrecision=True, wikidata_format=True):
    result = ""

    if year and month and day:
        precision = 11
    elif year and month and (not day):
        precision = 10
    elif year and (not month) and (not day):
        precision = 9
    else:
        raise ValueError("Year not specified or year and day specified while month not specified")

    if era == 'bc':
        if wikidata_format:
            result += "-" + padWithZeros(year, 4) # (in wikidata JSON format) 1 BC is -0001; 2 BC is -0002 etc
        else:
            result += "-" + padWithZeros(year - 1, 4) # (in ISO-8601) 1 BC is +0000; 2 BC is -0001 etc

    else:
        result += "+" + padWithZeros(year, 4)

    result += '-'

    if month:
        result += padWithZeros(month, 2)
    else:
        result += '00'

    result += '-'

    if day:
        result += padWithZeros(day, 2)
    else:
        result += '00'

    result += 'T00:00:00Z' # time not specified and in default timezone

    if withPrecision:
        result += '/' + str(precision)

    return result
