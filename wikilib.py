import requests
import json
import pprint
import re
import wiki_template
from apiRequest import getPage, getViewsPage, pageCoordinates
import urllib.parse
import parsing_dates

def get_page(lang, page):
    return requests.get('http://'+lang+'.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=' + page.strip()).content

def pretty(code):
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(code)


def get_first(d):
    return d[list(d.keys())[0]]

def remove_html_comments(s):
    return re.sub("(<!--.*?-->)", "", s, flags=re.MULTILINE)

def get_templates(page, possible_template_names): # Returns templates with given names
    possible_template_names = set(map(str.lower, possible_template_names))
    templateFilter = lambda template: remove_html_comments(wiki_template.parse_template(template)['template_name']).strip().lower() in possible_template_names
    templates = list(filter(templateFilter, wiki_template.get_templates_on_page(page)))
    return templates

def get_infoboxes(page): # Returns infoboxes
    isInfobox = lambda template: wiki_template.parse_template(template)['template_name'].lower().startswith('infobox')
    infoboxes = list(filter(isInfobox, wiki_template.get_templates_on_page(page)))
    return infoboxes


def get_infobox(page): # Returns infobox
    infoboxes = get_infoboxes(page)
    if len(infoboxes) == 0:
        return ''
    infobox = infoboxes[0]
    return infobox


def has_infobox(page, infobox_type):
    infobox = get_infobox(page)
    actual_infobox_category = wiki_template.parse_template(infobox)['template_name']
    if (infobox == '') or (re.search(r'infobox\s+' + infobox_type.lower(), actual_infobox_category.lower()) == None):
        return False
    else:
        return True

def parse_link(template): # returns [Link_Title, Description] or [Link_Title]
    start = template.find('[[')
    finish = template.find(']]')
    return template[start+2:finish].split('|')

def parse_birth_date_template(template):
    t = re.search(r'(\d+)\|(\d+)\|(\d+)', template)
    if t:
        t = t.groups()
        t = tuple(map(int, t))
        return (t[2], t[1], t[0])


def parse_death_date_template(template):
    return parse_birth_date_template(template)


def regex_ok(ex, s):
    return re.search(ex, s) != None


def parse_combatant_template(template):
    try:
        dashes = r'\u2012\u2013\u2014\u2015-'
        result = ''
        t = re.search(r'[\{](flag|flagcountry|flagicon|flag icon|flagu|army|navy|plainlist)\|([\w\s\(\),'+dashes+r']+)[\|\}]', template, re.IGNORECASE)
        if t:
            t = t.groups()
            if len(t) > 1:
                result = t[1]

        t = re.findall(r'\[([\w\s\(\),\|'+dashes+r']+)\]', template)
        for s in t:
            #print(s)
            if 'file:' not in s:
                t2 = re.search(r'([\w\s\(\),'+dashes+r']+)\|', s)
                if t2:
                    t2 = t2.groups()
                    result = t2[0]
                else:
                    result = s
        page = getPage(result, followRedirects=False).lower()
        #print('REDIRECT PAGE:')
        #print(page)
        t = re.search(r'#redirect\s+\[\[([\w\s\(\),'+dashes+r']+)\]\]', page, re.IGNORECASE)
        if t:
            t = t.groups()
            if len(t) > 0:
                result = t[0]
        return result
    except:
        return ''

def parse_place_template(template):
    dashes = r'\u2012\u2013\u2014\u2015-'
    return re.findall(r'\[\[([\w\s(\),'+dashes+r']+)\]\]', template) + re.findall(r'\|([\w\s(\),'+dashes+r']+)\]\]', template)

def get_place_text(template):
    try:
       return ', '.join(parse_place_template(template['place']))
    except Exception as e:
        return template
    
def extract_combatants_from_template(parsed_template):
    combatants = []
    for s1 in ['1', '2', '3']:
        for s2 in ['', 'a', 'b', 'c', 'd']:
            cmb = 'combatant' + s1 + s2
            if cmb in parsed_template:
                combatants.append(parse_combatant_template(parsed_template[cmb]))
    return combatants

def parse_infobox_military_conflict(page):
    parsed_template = wiki_template.parse_template(page)['options']
    result = {}
    if 'conflict' in parsed_template:
        result['conflict'] = parsed_template['conflict']
    if 'date' in parsed_template:
        result['date_raw'] = parsed_template['date']
        result['date'] = parsing_dates.drop_date_precision(parsing_dates.work_on_dates(parsed_template['date']))
    if 'place' in parsed_template:
        result['place_raw'] = parsed_template['place']
        result['place'] = get_place_text(parsed_template['place'])
    if 'partof' in parsed_template:
        pl = parse_link(parsed_template['partof'])
        if len(pl) == 2:
            result['partof'] = {'link': pl[0], 'description': pl[1]}
        elif len(pl) == 1:
            result['partof'] = {'link': pl[0]}
    combatants = extract_combatants_from_template(parsed_template)
    result['combatants'] = combatants
    return result

def parse_infobox_building(page):
    parsed_template = wiki_template.parse_template(page)['options']
    result = {
        'name' : parsed_template.get('name', ''),
        'date_raw' : parsed_template.get('completion_date', ''),
        'date' : parsing_dates.drop_date_precision(parsing_dates.work_on_dates(parsed_template.get('completion_date', ''))),
        'place_raw' : parsed_template.get('location', ''),
        'place' : get_place_text(parsed_template.get('location', '')),
    }
    return result


def parse_list_template(template):
    res = []
    for s in re.findall(r'\[\[[^\]]+\]\]', template):
        res.append(s[2:len(s)-2])
    return res


def parse_infobox_scientist(page):
    # return wiki_template.parse_template(page)['options']
    parsed_template = wiki_template.parse_template(page)['options']
    result = {
        'name'       : 'The Birth of ' + parsed_template.get('name', ''),
        'birth_date' : parsed_template.get('birth_date', ''),
        'death_date' : parsed_template.get('death_date', ''),
        'fields'     : parsed_template.get('fields', ''),
    }
    return result

def get_views(name):
    try:
        views_dict = json.loads(getViewsPage(name))
        result = 0
        for day_views in views_dict['items']:
            result += day_views['views']
        return result
    except Exception:
        return None

def get_page_coord(name, placeInfo = None):
    result = pageCoordinates(name)
    if result:
        return result
    related_coords = []
    if placeInfo:
        for link in re.findall(r'(\[\[.*?\]\])', placeInfo):
            link_dest = link.split('|')[0].strip('[]') # ToDo: not all link templates are parsed here
            # print(link_dest)
            link_coords = pageCoordinates(link_dest)
            if link_coords:
                related_coords.append(link_coords)
    most_exact = min(related_coords, key=lambda coords: coords.get('rad', float('inf')), default=None)
    return most_exact
