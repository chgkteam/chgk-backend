require 'csv'
def check_date_consistency(date)
  precision = Integer(date.split('/').last)
  year = Integer(date[0,5],10)
  month = Integer(date[6,2],10)
  day = Integer(date[9,2],10)
  if (month > 0) && (day > 0)
    return precision == 11
  elsif (month > 0)
    return precision == 10
  else
    return precision == 9
  end
end

sources = ['S143', 'Q328', 'S813', '+2018-03-23T00:00:00Z/11'] # <imported from> <English wikipedia>; <retrieved> 23 March 2018

csv = CSV.new($stdin, col_sep:"\t")
csv.readlines.drop(1).select{|row|
  status = row[1].downcase
  status == 'ok'
}.each{|row|
  idx,status,title, wikidata_id, original_date, parsed_date_text, point_in_time, start_time, end_time = *row
  point_in_time = nil  if point_in_time && point_in_time.empty?
  start_time = nil  if start_time && start_time.empty?
  end_time = nil  if end_time && end_time.empty?
  $stderr.puts("Please check precision for #{wikidata_id} start time #{start_time}")  if start_time && !check_date_consistency(start_time)
  $stderr.puts("Please check precision for #{wikidata_id} end time #{end_time}")  if end_time && !check_date_consistency(end_time)
  $stderr.puts("Please check precision for #{wikidata_id} point in time #{point_in_time}")  if point_in_time && !check_date_consistency(point_in_time)
  raise  if point_in_time && (start_time || end_time)
  puts [wikidata_id, 'P585', point_in_time, *sources].join("\t")  if point_in_time
  puts [wikidata_id, 'P580', start_time, *sources].join("\t")  if start_time
  puts [wikidata_id, 'P582', end_time, *sources].join("\t")  if end_time
}
