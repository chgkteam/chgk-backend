import peewee
import sqlite3
import datetime
import sys
import hashlib

cache_db = peewee.SqliteDatabase('wikicache.db')

class CachingBaseModel(peewee.Model):
    class Meta:
        database = cache_db

class CacheType(CachingBaseModel):
    name = peewee.CharField(unique=True, index=True)

class CachedPage(CachingBaseModel):
    digest          = peewee.CharField(index=True, unique=True, primary_key=True)
    content         = peewee.TextField()
    timestamp       = peewee.TimestampField(default=datetime.datetime.now)

# class APIRequestCache(CachingBaseModel):
#     cachedPage      = peewee.ForeignKeyField(CachedPage, index=True)
#     uri             = peewee.CharField()
#     params          = peewee.CharField() # ToDo: use JSONField
#     request_type    = peewee.ForeignKeyField(CacheType, index=True)

# class WikipageCache(CachingBaseModel):
#     cachedPage      = peewee.ForeignKeyField(CachedPage, index=True)
#     uri             = peewee.CharField()
#     title           = peewee.CharField(index=True)
#     follow_redirect = peewee.BooleanField()

def createCachingTables():
    tables = [
        CacheType,
        CachedPage,
        # APIRequestCache,
        # WikipageCache,
    ]
    cache_db.connect()
    cache_db.create_tables(tables, safe=True)


def digest(uri):
    return hashlib.sha512( uri.encode('utf-8') ).hexdigest()

def dbCacheLoad(digest):
    record = CachedPage.select().where(CachedPage.digest == digest).first()
    if record:
        return (record.content, True)
    else:
        return (None, False)

def dbCacheStore(content, digest):
    fields = {
        'content': content,
        'digest': digest
    }
    CachedPage.insert(fields).on_conflict_replace().execute()

def dbCacheLoaderByURI(uriFunction):
    def loader(*args, **kwargs):
        return dbCacheLoad(digest(uriFunction(*args, **kwargs)))
    return loader

def dbCacheStorerByURI(uriFunction):
    def storer(content, *args, **kwargs):
        dbCacheStore(content, digest(uriFunction(*args, **kwargs)))
    return storer

def dbCachingPairByURI(uriFunction):
    return (dbCacheLoaderByURI(uriFunction), dbCacheStorerByURI(uriFunction))


# def get_content_by_title(title, follow_redirect=True):
#     return PageCache.get((PageCache.title == title) & (PageCache.follow_redirect == follow_redirect))

# def get_content_by_digest(digest):
#     return CachedPage.get(CachedPage.digest == digest)
