import re
import html_cleanup
from wikidata_dates import padWithZeros

length_month = [None, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

month_numbers = {
    'january': 1, 'jan': 1,
    'february': 2, 'feb': 2,
    'march': 3,  'mar': 3,
    'april': 4, 'apr': 4,
    'may': 5,
    'june': 6, 'jun': 6,
    'july': 7, 'jul': 7,
    'august': 8, 'aug': 8,
    'september': 9, 'sep': 9,
    'october': 10, 'oct': 10,
    'november': 11, 'nov': 11,
    'december': 12, 'dec': 12,
    '1': 1, '01': 1,  '2': 2, '02': 2,  '3': 3, '03': 3,  '4': 4, '04': 4,
    '5': 5, '05': 5,  '6': 6, '06': 6,  '7': 7, '07': 7,  '8': 8, '08': 8,
    '9': 9, '09': 9,  '10': 10,  '11': 11,  '12': 12,
    None: None
}

month_names = [None, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

variation_era = {
    'bc': 'bc', 'b.c.': 'bc', 'bce': 'bc', 'b.c.e.': 'bc',
    'ad': 'ad', 'a.d.': 'ad', 'c.e.': 'ad', 'ce': 'ad',
    None: None,
}

def capturing_regexp(capture_name, regexp):
    return r'(?P<' + capture_name + r'>' + regexp + ')'
def noncapturing_regexp(regexp):
    return r'(?:' + regexp + r')'
def bounded_regexp(regexp):
    return r'\b' + noncapturing_regexp(regexp) + r'\b'

day_pat = bounded_regexp(
    capturing_regexp('day', r'0?[1-9]|[12][0-9]|3[01]') +
    noncapturing_regexp('-?' + noncapturing_regexp('st|nd|rd|th')) + '?'
)

month_number_pat = capturing_regexp('month', bounded_regexp(r'\d{1,2}'))
month_pat = capturing_regexp('month', bounded_regexp(r'january|february|march|april|may|june|july|august|september|october|november|december|jan|feb|mar|apr|may|jun|jul|aug|sep|nov|dec'))
year_pat = capturing_regexp('year', bounded_regexp(r'\d{1,4}'))

# ToDo: fix era template (put \b at the end of eras, which aren't ended with a dot.)
era_pat = capturing_regexp('era', r'\b(b\.?c\.?|a\.?d\.?|b\.?c\.?e\.?|c\.?e\.?)')

# ToDo: parse "present" and ongoing
optional_comma = r'(?:\s+|,\s+)'
date_pattern_list = [
    r'\{\{([Ss]tart|[Ee]nd) date\s*\|\s*' + year_pat + r'}}',
    r'\{\{([Ss]tart|[Ee]nd) date\s*\|\s*' + year_pat + r'\s*\|\s*' + month_number_pat + r'\}\}',
    r'\{\{([Ss]tart|[Ee]nd) date\s*\|\s*' + year_pat + r'\s*\|\s*' + month_number_pat + r'\|' + day_pat + r'(\|df=y(es)?)?' + r'\}\}',
    day_pat + r'\s+' + month_pat + optional_comma + year_pat + r'\s*' + era_pat + '?',
    month_pat + r'\s+' + day_pat + optional_comma + year_pat + r'\s*' + era_pat + '?',
    year_pat + optional_comma + month_pat + r'\s*' + day_pat + r'\s*' + era_pat + '?',
    year_pat + r',\s' + day_pat + r'\s+' + month_pat + r'\s*' + era_pat + '?',
    year_pat + r'\s+' + month_pat + r'\s+' + era_pat,
    month_pat + optional_comma + year_pat + r'\s+' + era_pat,
    day_pat + optional_comma + month_pat,
    month_pat + optional_comma + day_pat,
    day_pat + optional_comma + year_pat + r'\s*' + era_pat,
    year_pat + r'\s+' + era_pat,

    day_pat + optional_comma + year_pat,
    day_pat,
    year_pat + optional_comma + month_pat,
    month_pat + optional_comma + year_pat,
    month_pat,
    year_pat,
]


def none_or_int(x):
    if x != None:
        return int(x)
    else:
        return None

def parse_date(date_string):
    try:
        date_string = date_string.strip()
        date_string = re.sub(r'\s+', ' ', date_string)
        date_string = re.sub(r' , ?', ', ', date_string)

        for pattern in date_pattern_list:
            match = re.search(pattern, date_string)
            if match:
                match = match.groupdict()
                day = none_or_int(match.get('day', None))
                month = month_numbers[match.get('month', None)]
                year = none_or_int(match.get('year', None))
                era = variation_era[match.get('era', None)]
                return [day, month, year, era]

        return [None, None, None, None]
    except Exception:
        return [None, None, None, None]

# parse_wiki_date_range('january - march 2013 [[Julian Calendar]]<br/><i>some text</i') --> ([None, 1, 2013, None], [None, 3, 2013, None])
def parse_wiki_date_range(wiki_markup):
    wiki_markup = wiki_markup.lower()
    wiki_markup = wiki_markup.replace('&nbsp;', ' ')
    dashes = r'[\u2012\u2013\u2014\u2015\\/-]|&[nm]dash;|\bto\b|\{\{(spaced )?(en ?|n)?dash\}\}|\{\{snd\}\}'
    wiki_markup = re.sub(dashes, '-', wiki_markup)
    wiki_markup = re.sub('-+', '-', wiki_markup)

    wiki_markup = re.sub(r'\{\{start-date\s*\|\s*([^|]+)(\|[^}]+)?\}\}', r'\1', wiki_markup)
    wiki_markup = re.sub(r'\{\{end-date\s*\|\s*([^|]+)(\|[^}]+)?\}\}', r'\1', wiki_markup)
    result = parse_date_string(wiki_markup)
    if not result:
        wiki_markup = html_cleanup.iterated_remove_tags(wiki_markup)
        result = parse_date_string(wiki_markup)
    for date in result:
        if not date[2]:
            return None # we didn't recognize even a year
    return result

# parse_date_range('1 jan - 5 feb 2013')
def parse_date_string(date_string):
    try:
        if date_string.find('-') != -1:
            return parse_date_range(date_string)
        else:
            return [parse_date(date_string)]
    except Exception:
        return []

# parse_date_range('1 jan - 5 feb 2013')
def parse_date_range(date_range_string):
    range_strings = date_range_string.split('-')
    from_date = parse_date(range_strings[0])
    to_date = parse_date(range_strings[1])
    # print(from_date, to_date)
    from_date, to_date = complete_date(from_date, to_date)
    from_date, to_date = order_dates(from_date, to_date)
    return [from_date, to_date]

# num_digits(23) => 2
# num_digits(123) => 3
# num_digits(5678) => 4
def num_digits(value):
    x = value
    i = 1
    while x // (10**i) > 0:
        i += 1
    return i

# 1914 - 17
def complete_year(base_year, year_to_complete):
    if (base_year > year_to_complete):
        age_size = 10 ** num_digits(year_to_complete) # 100
        century = (base_year // age_size) * age_size # 1914 --> 1900
        year_to_complete += century # 17 --> 1917
    return year_to_complete

# 19 - 29 BC
# [19, None, None, 'bc'] align with [None, None, 29, 'bc'] --> [None, None, 19, 'bc']
def align_date_positions(date_to_align, basis_date):
    day, month, year, era = date_to_align
    basis_day, basis_month, basis_year, era = date_to_align
    if day and (not month) and (not year) and (not basis_day) and (not basis_month) and basis_year:
        return [None, None, day, era]

# complete_date([1,1, None, None], [5, 2, 2013, None]) --> ([1, 1, 2013, None], [5, 2, 2013, None])
def complete_date(date_1, date_2):
    date_1 = date_1[:]
    date_2 = date_2[:]

    # in date `19 - 29` numbers are not days but years
    if (date_1[0] != None) and (date_1[2] == None) and \
      (date_2[0] != None) and (date_2[2] == None):
        date_1[0], date_1[2] = None, date_1[0]
        date_2[0], date_2[2] = None, date_2[0]

    # `19 - 1234` should be treated as 19 year to 1234 year
    if (date_1[0] != None) and (date_1[1] == None) and (date_2[1] == None) and (date_1[2] == None):
        date_1[0], date_1[2] = None, date_1[0]

    completed_date_1 = date_1[:]
    completed_date_2 = date_2[:]

    # the simplest case:
    # [1, None, None, None], [12, 3, 1945, None] --> [1, 3, 1945, None], [12, 3, 1945, None]
    i = 2;
    while (((date_1[i] != None) and (date_2[i] == None)) or ((date_1[i] == None) and (date_2[i] != None))) and (i >= 0):
        if date_1[i] != None:
            completed_date_2[i] = date_1[i]
        else:
            completed_date_1[i] = date_2[i]
        i -= 1

    # put BC/AD to other side
    if date_1[3] == None:
        completed_date_1[3] = date_2[3]

    # if month not set (at both sides) then `year` was misinterpreted as `day`; fix it and make it `year` again
    if (completed_date_2[1] == None) and (completed_date_2[0] != None):
        completed_date_2[2] = completed_date_2[0]
        completed_date_2[0] = None

    # 1914 - 17 --> 1914 - 1917
    if (completed_date_1[3] == 'ad') or (completed_date_1[3] == None):
        completed_date_2[2] = complete_year(completed_date_1[2], completed_date_2[2])
    return completed_date_1, completed_date_2

def is_leap_year(year):
    return (year % 4 == 0) and ((year % 100 != 0) or (year % 400 == 0))

def to_signed_year(date):
    result = date[0:2] + [signed_year(date)]
    return result

def signed_year(date):
    if date[3] == 'bc':
        return -date[2]
    else:
        return  date[2]

def date_wikidata_precision(date):
    if date[1] == None: # month not specified
        return 9
    elif date[0] == None:  # day mot specified
        return 10
    else:
        return 11

# set date to the first day of the year or the last day of the month
def complete_date_leftside(date):
    result = date[:]
    if result[1] == None: # month not specified
        result[0] = 1
        result[1] = 1
    elif result[0] == None:  # day mot specified
        result[0] = 1
    return result

# set date to the last day of the year or the last day of the month
def complete_date_rightside(date):
    result = date[:]
    if result[1] == None: # month not specified
        result[0] = 31
        result[1] = 12
    elif result[0] == None: # day not specified
        if (result[1] == 2) and is_leap_year(result[2]) :
            result[0] = 29
        else:
            result[0] = length_month[result[1]]
    return result

# in BC era dates are often reported in reverse order: 12bc to 34bc (means -34 to -12 years)
def order_dates(date_from, date_to):
    if signed_year(date_from) > signed_year(date_to):
        return date_to, date_from
    else:
        return date_from, date_to

# extend_date([None, 2, 30, 'bc'], [None, 5, 20, 'ad']) --> ([1, 2, -30], [31, 5, 20])
def extend_date(date_from, date_to):
    date_from = to_signed_year(date_from)
    date_to = to_signed_year(date_to)

    date_from = complete_date_leftside(date_from) + [date_wikidata_precision(date_from)]
    date_to = complete_date_rightside(date_to) + [date_wikidata_precision(date_to)]

    return date_from, date_to

# work_on_dates('january - march 2013 [[Julian Calendar]]<br/><i>some text</i') --> ([1, 1, 2013], [31, 3, 2013])
def work_on_dates(wiki_markup):
    try:
        return extend_date(*parse_wiki_date_range(wiki_markup))
    except Exception:
        return [[None, None, None, None], [None, None, None, None]]

def drop_date_precision(date):
    try:
        date_1 = date[0][0:3]
        date_2 = date[1][0:3]
        return date_1, date_2
    except Exception:
        return [[None, None, None], [None, None, None]]

def format_date(date):
    day, month, year, era = date
    result = ""
    if month:
        if day:
            result = str(day) + " " + month_names[month] + ", "
        else:
            result = month_names[month] + ", "
    if year:
        result += str(year)
    if era == 'bc':
        result += ' BC'
    return result

def possible_date_formattings(date):
    variants = []
    day, month, year, era = date
    year = str(year)
    if era == 'bc':
        year += ' BC'
    if month:
        for year_sep in [" ", ", "]:
            if day:
                for day_sep in [" ", ", "]:
                    variants.append(str(day) + day_sep + month_names[month] + year_sep + year)
                    variants.append(month_names[month] + day_sep + str(day) + year_sep + year)
            else:
                variants.append(month_names[month] + year_sep + year)
    else:
        variants.append(year)
    variants.append(date_template_format(date, 'Start date'))
    variants.append(date_template_format(date, 'Start date', day_month_padding=True))
    return list(set(variants))

def date_template_format(date, template_name, day_month_padding=False):
    day, month, year, era = date
    year, month, day = str(year), str(month), str(day)
    if era == 'bc':
        year += ' BC'
    if month:
        if day:
            if day_month_padding:
                result = '{{' + template_name + '|' + year + '|' + padWithZeros(month,2) + '|' + padWithZeros(day,2) + '}}'
            else:
                result = '{{' + template_name + '|' + year + '|' + month + '|' + day + '}}'
        else:
            if day_month_padding:
                result = '{{' + template_name + '|' + year + '|' + padWithZeros(month,2) + '}}'
            else:
                result = '{{' + template_name + '|' + year + '|' + month + '}}'
    else:
        result = '{{' + template_name + '|' + year + '}}'
    return result

def possible_age_in_years_templates(date_from, date_to):
    day_from, month_from, year_from, era_from = date_from
    day_to, month_to, year_to, era_to = date_to
    if (era_from == 'bc') or (era_to == 'bc'):
        return []
    variants = []
    if day_from and day_to:
        variants.append("{{Age in years, months, weeks and days|year1="+str(year_from)+"|month1="+str(month_from)+"|day1="+str(day_from)+"|year2="+str(year_to)+"|month2="+str(month_to)+"|day2="+str(day_to)+"}}")
        variants.append("{{Age in years, months, weeks and days|month1="+str(month_from)+"|day1="+str(day_from)+"|year1="+str(year_from)+"|month2="+str(month_to)+"|day2="+str(day_to)+"|year2="+str(year_to)+"}}")
        variants.append("{{Age in years, months, weeks and days|year1="+str(year_from)+"|month1="+padWithZeros(month_from,2)+"|day1="+padWithZeros(day_from,2)+"|year2="+str(year_to)+"|month2="+padWithZeros(month_to,2)+"|day2="+padWithZeros(day_to,2)+"}}")
        variants.append("{{Age in years, months, weeks and days|month1="+padWithZeros(month_from,2)+"|day1="+padWithZeros(day_from,2)+"|year1="+str(year_from)+"|month2="+padWithZeros(month_to,2)+"|day2="+padWithZeros(day_to,2)+"|year2="+str(year_to)+"}}")
    elif month_from and month_to and not (day_from or day_to):
        variants.append("{{Age in years, months, weeks and days|year1="+str(year_from)+"|month1="+str(month_from)+"|year2="+str(year_to)+"|month2="+str(month_to)+"}}")
        variants.append("{{Age in years, months, weeks and days|month1="+str(month_from)+"|year1="+str(year_from)+"|month2="+str(month_to)+"|year2="+str(year_to)+"}}")
        variants.append("{{Age in years, months, weeks and days|year1="+str(year_from)+"|month1="+padWithZeros(month_from,2)+"|year2="+str(year_to)+"|month2="+padWithZeros(month_to,2)+"}}")
        variants.append("{{Age in years, months, weeks and days|month1="+padWithZeros(month_from,2)+"|year1="+str(year_from)+"|month2="+padWithZeros(month_to,2)+"|year2="+str(year_to)+"}}")
    elif year_from and year_to and not (month_from or month_to):
        variants.append("{{Age in years, months, weeks and days|year1="+str(year_from)+"|year2="+str(year_to)+"}}")
    return variants

def possible_date_range_formattings_fixed_sep(date_from, date_to, year_sep, day_sep, range_sep):
    variants = []
    day_from, month_from, year_from, era_from = date_from
    day_to, month_to, year_to, era_to = date_to
    year_from = str(year_from)
    year_to = str(year_to)
    if era_from == 'bc':
        year_from += ' BC'
    if era_to == 'bc':
        year_to += ' BC'

    for date_from_formatting in possible_date_formattings(date_from):
        for date_to_formatting in possible_date_formattings(date_to):
            variants.append(date_from_formatting + range_sep + date_to_formatting)

    if year_from and (year_from == year_to):
        if month_from and month_to:
            if (month_from == month_to):
                if day_from and day_to:
                    daymonth_1 = str(day_from) + range_sep + str(day_to) + day_sep + month_names[month_from]
                    daymonth_2 = month_names[month_from] + day_sep + str(day_from) + range_sep + str(day_to)
                    variants.append(daymonth_1 + year_sep + year_from)
                    variants.append(daymonth_2 + year_sep + year_from)
                    variants.append(year_from + year_sep + daymonth_1)
                    variants.append(year_from + year_sep + daymonth_2)
            else:
                if (not day_from) and (not day_to):
                    variants.append(month_names[month_from] + range_sep + month_names[month_to] + year_sep + year_from)
                    variants.append(year_from + year_sep + month_names[month_from] + range_sep + month_names[month_to])

            if day_from and day_to:
                daymonth_1 = str(day_from) + day_sep + month_names[month_from] + range_sep + str(day_to) + day_sep + month_names[month_to]
                daymonth_2 = month_names[month_from] + day_sep + str(day_from) + range_sep + month_names[month_to] + day_sep + str(day_to)
                variants.append(daymonth_1 + year_sep + year_from)
                variants.append(daymonth_2 + year_sep + year_from)
                variants.append(year_from + year_sep + daymonth_1)
                variants.append(year_from + year_sep + daymonth_2)

    variants.append(date_template_format(date_from, 'Start date') + range_sep + date_template_format(date_to, 'End date'))
    variants.append(date_template_format(date_from, 'Start date', day_month_padding=True) + range_sep + date_template_format(date_to, 'End date', day_month_padding=True))
    return list(set(variants))

def possible_date_range_formattings(date_from, date_to):
    variants = []
    age_templates = possible_age_in_years_templates(date_from, date_to)
    age_templates_w_sep = set()
    for age_template in age_templates:
        for template_space_before in ["", " ",]:
            for template_sep in ["", "<br>", "<br/>", "<br />",]:
                age_templates_w_sep.add(template_space_before + template_sep + "(" + age_template + ")")
                age_templates_w_sep.add(template_space_before + template_sep + age_template)
    for year_sep in [" ", ", "]:
        for day_sep in [" ", ", "]:
            for minus_char in ["-", "\u2012","\u2013","\u2014","\u2015", ' to ', '&dash;', '&ndash;', '&endash', '{{ndash}}', '{{spaced dash}}', '{{spaced en dash}}', '{{spaced ndash}}', '{{spaced endash}}']:
                for range_sep in [minus_char, " " + minus_char + " ", "&nbsp;" + minus_char + " "]:
                    for date_range_variant in possible_date_range_formattings_fixed_sep(date_from, date_to, year_sep, day_sep, range_sep):
                        variants.append(date_range_variant)
                        for age_template in age_templates_w_sep:
                            variants.append(date_range_variant + age_template)
    return list(set(variants))
