categoryName="$1"
infoboxType="$2"
python3 categoryLister.py "$categoryName" yes \
  | python3 parseToDB.py "$infoboxType"
