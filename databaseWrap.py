import peewee
import sqlite3
import datetime
import sys

db = peewee.SqliteDatabase('events.db')

class BaseModel(peewee.Model):
    class Meta:
        database = db

class Coord(BaseModel):
    id = peewee.PrimaryKeyField(index=True, unique=True, primary_key=True)
    lat = peewee.DoubleField()
    lng = peewee.DoubleField()
    radius = peewee.DoubleField()
    comment = peewee.TextField()

class EventType(BaseModel):
    id = peewee.PrimaryKeyField(index=True, unique=True, primary_key=True)
    name_key = peewee.TextField(index=True, unique=True) # ToDo: this field introduced temporarily, remove it
    name = peewee.TextField()

class Event(BaseModel):
    id = peewee.PrimaryKeyField(index=True, unique=True, primary_key=True)
    name = peewee.TextField()
    url = peewee.TextField()
    description = peewee.TextField()
    coordId = peewee.ForeignKeyField(Coord, null=False)
    dateStart = peewee.DateField(index=True)
    dateEnd = peewee.DateField()
    partOf = peewee.ForeignKeyField('self', related_name='parts', null=True)
    pageViews = peewee.IntegerField(default=0, index=True)
    eventType = peewee.ForeignKeyField(EventType)

class Country(BaseModel):
    id = peewee.PrimaryKeyField(index=True, unique=True, primary_key=True)
    name = peewee.TextField()
    eventId = peewee.ForeignKeyField(Event)

def createTables():
    db.connect()
    db.create_tables([Coord, EventType, Event, Country], safe=True)
