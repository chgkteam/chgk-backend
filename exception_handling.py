def ignoreExceptionDecorator(defaultResult = None):
    '''
    Wrapped function intercepts an exception and discards it.
    In such a case, result is calculated by defaultResult callback
    If defaultResult is a value, it's just returned
    '''
    if callable(defaultResult):
        defaultResultFn = defaultResult
    else:
        defaultResultFn = lambda *args, **kwargs: defaultResult
    def actualDecorator(func):
        def wrappedFunction(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                return defaultResultFn(*args,**kwargs)
        return wrappedFunction
    return actualDecorator
