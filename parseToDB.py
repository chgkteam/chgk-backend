from contextlib import redirect_stdout
import apiRequest
import io
import sys
import wikilib
from dateConverters import ct, cf
import databaseWrap
from databaseWrap import Event, Coord, Country, EventType
import datetime
import urllib.parse
import json

URL_BASE = 'https://en.wikipedia.org/wiki/'

def infobox_parser(infoboxType):
    if (infoboxType == 'military_conflict'):
        return wikilib.parse_infobox_military_conflict
    elif (infoboxType == 'building'):
        return wikilib.parse_infobox_building

def create_coordinates(infoboxData, name):
    coords = wikilib.get_page_coord(name, infoboxData.get('place_raw', None))
    if coords is None:
        return None
    coordComment = infoboxData.get('place', '')
    radius = (coords['rad'] or 0.0)
    record = Coord.create(lat=coords['lat'], lng=coords['lng'], comment=coordComment, radius=radius)
    return record

# ToDo: This is temporary solution, remove it later
def eventTypeRecord(eventType):
    if eventType == 'military_conflict':
        return EventType.get_or_create(name_key='military_conflict', name='Военные конфликты')[0]
    elif eventType == 'building':
        return EventType.get_or_create(name_key='building', name='Здания')[0]
    
def create_event(infoboxData, name, infoboxType):
    coord = create_coordinates(infoboxData, name)
    if coord is None:
        return None
    # partOf = None
    try:
        dateStart, dateEnd = map(lambda d: ct(list(reversed(d))), infoboxData['date'])
    except Exception as e:
        print(e, 'exception occured on', infoboxData, file=sys.stderr)
        dateStart = datetime.date(1, 1, 1)
        dateEnd = datetime.date(1, 1, 1)
    # ToDo: remove not-NULL constraint from DB and drop this default value
    pageViews = wikilib.get_views(name) or 0
    description = apiRequest.getSummary(name)
    url = URL_BASE + name.replace(' ', '_')
    record_values = {
        'name': name,
        'url': url,
        'coordId': coord,
        'dateStart': dateStart,
        'dateEnd': dateEnd,
        'description': description,
        'pageViews': pageViews,
        'eventType': eventTypeRecord(infoboxType)
    }
    event = Event.create(**record_values)
    return event
                
def main(stdin = sys.stdin):
    infoboxType = sys.argv[1]
    databaseWrap.createTables()
    for line in stdin.readlines():
        name = line.strip()
        print(name, file=sys.stderr)
        if name.strip() == '':
            continue
        if Event.select().where(Event.name == name).exists():
            continue
        page = apiRequest.getPage(name)
        infoboxData = infobox_parser(infoboxType)(wikilib.get_infobox(page))
        event = create_event(infoboxData, name, infoboxType)
        if event:
            for combatant in infoboxData.get('combatants', []):
                Country.create(name=combatant, eventId=event)

if __name__ == '__main__':
    main()
