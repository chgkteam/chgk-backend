import urllib.request
import urllib.parse
import urllib.error
import sys
import os.path
import json
import copy
import re
import apiRequest
import wikilib
import caching_to_db

def pagesList(categoryName, cmtype='page'):
    res = json.loads( apiRequest.categoryPages(categoryName, cmtype=cmtype) )
    members = res['query']['categorymembers']
    while 'continue' in res:
        res = json.loads( apiRequest.categoryPages(categoryName, cmtype=cmtype, cmcontinue=res['continue']['cmcontinue']) )
        members += res['query']['categorymembers']
    return members

def subcategoriesList(categoryName):
    return pagesList(categoryName, cmtype='subcat')

# Note that visited should be shared across all recursive calls in all branches, so it is made in mutable way
# (it's possible to rewrite this in immutable way, but it's not as simple as it seems)
def recursivePagesList(categoryName, depthLimit=-1, pageTest=(lambda _: True), fullPath=False, visited=set(), path=()):
    '''
    Yields pages in category and its subcategories.
    If `pageTest` is specified, yield only pages which satisfy specified condition
    If `fullPath` is set to true, whole path across subcategories to an article will be yielded as a tuple
    `depthLimit` specifies a number of subcategory levels, which this method should discover. Set depthLimit to -1 for unbounded depth
    '''
    if (depthLimit == 0) or (categoryName in visited):
        return
    visited.add(categoryName)

    indent = '\t' * len(path)
    print(indent  + ' --> ' + categoryName, file = sys.stderr)

    for page in pagesList(categoryName):
        pageTitle = page['title']
        if pageTitle in visited:
            continue
        content = apiRequest.getPage(pageTitle)
        visited.add(pageTitle)
        if pageTest(content):
            if fullPath:
                yield path + (categoryName, pageTitle, )
            else:
                yield pageTitle

    for category in subcategoriesList(categoryName):
        subcategoryName = category['title'] # .replace(' ', '_')
        yield from recursivePagesList( subcategoryName,
                                       depthLimit = depthLimit - 1,
                                       pageTest = pageTest,
                                       fullPath = fullPath,
                                       visited = visited,
                                       path = (path + (categoryName, ))
                                      )

def main():
    caching_to_db.createCachingTables()
    categoryName = 'Category:' + sys.argv[1]
    fullPath = (len(sys.argv) > 2) and (sys.argv[2].lower() == 'yes')

    pageTest = lambda page: wikilib.has_infobox(page, 'military conflict')

    filename = os.path.join('logs', '{}.log'.format(categoryName))
    with open(filename, 'w') as categoryLogfile:
        if fullPath:
            for path in recursivePagesList(categoryName, fullPath=True, pageTest=pageTest):
                print(path[-1])
                print(' -> '.join(path), file = categoryLogfile)
        else:
            for page in recursivePagesList(categoryName, fullPath=False, pageTest=pageTest):
                print(page)
                print(page, file = categoryLogfile)

if __name__ == '__main__':
    main()
