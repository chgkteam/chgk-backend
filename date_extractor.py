import sys
import re
import csv
import traceback

import apiRequest
import wikilib
import wiki_template
import parsing_dates
import wikidata_dates
from wikidata_dates import wikidata_to_date_with_era, extractTimeFromClaim, filterClaims, dateToWikidataFormat

def get_date_infos_for_infobox(parsed_template):
    if 'date' not in parsed_template:
        return {'status': 'no-attribute-date', 'date_raw': '', 'date_formatted': '', 'dates': []}
    if len(parsed_template['date'].strip()) == 0:
        return {'status': 'empty-attribute-date', 'date_raw': '', 'date_formatted': '', 'dates': []}

    date_raw = parsed_template['date']
    dates = parsing_dates.parse_wiki_date_range(date_raw)
    if not dates or len(dates) == 0:
        return {'status': 'not-parsed', 'date_raw': date_raw, 'date_formatted': '', 'dates': []}

    if len(dates) == 1:
        date = dates[0]
        date_formatted = parsing_dates.format_date(date)
        possible_formattings = parsing_dates.possible_date_formattings(date)
    elif len(dates) == 2:
        date_from, date_to = dates
        date_formatted = parsing_dates.format_date(date_from) + ' - ' + parsing_dates.format_date(date_to)
        # print(date_from, date_to, file=sys.stderr)
        # print(dateToWikidataFormat(*date_from, withPrecision=True), '--', dateToWikidataFormat(*date_to, withPrecision=True), file=sys.stderr)
        possible_formattings = parsing_dates.possible_date_range_formattings(date_from, date_to)

    possible_formattings = list(map(str.lower, possible_formattings))
    # parsing_dates.date_range_formattings(date_from, date_to)

    cleaned_date_row = re.sub('\s*([\u2012\u2013\u2014\u2015-]|&[nm]dash;)\s*', ' - ', str.lower(date_raw))

    status = '?'
    if cleaned_date_row in possible_formattings:
        status = '+'
        # Status + is not always correct.
        # E.g. in some articles year is specified only in page title but not in date option.
        # In that case days will be interpreted as years which is incorrect
    return {'status': status, 'date_raw': date_raw, 'date_formatted': date_formatted, 'dates': dates}

def main(stdin = sys.stdin):
    already_processed = set()
    with open('infos_to_check.tsv') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            if row[1] not in ['?', '+']:
                already_processed.add(row[2])

    print_detailed_info = ('--details' in sys.argv)
    csv_writer = csv.writer(sys.stdout, delimiter='\t')
    csv_writer.writerow(['idx', 'status', 'title', 'Wikidata-id', 'original date', 'parsed date', 'point in time', 'start time', 'end time'])
    idx = 0
    for line in stdin.readlines():
        name = line.strip()
        if name == '':
            continue
        idx += 1
        name = re.sub(' ', '_', name, count = 0)

        if name in already_processed:
            continue

        wikidata_has_claims = None
        try:
            wikidata_id = apiRequest.getWikidataID(name)
            wikidata_claims = apiRequest.getWikidataProperties(wikidata_id)

            start_time_claims = filterClaims(wikidata_claims, 'P580')
            end_time_claims = filterClaims(wikidata_claims, 'P582')
            point_in_time_claims = filterClaims(wikidata_claims, 'P585')

            if (len(start_time_claims) > 0) or (len(end_time_claims) > 0) or (len(point_in_time_claims) > 0):
                wikidata_has_claims = True # continue # don't modify wikidata if it already has date claims (for now)
            if wikidata_has_claims:
                continue
                status = 'skip'

            date_infos = None
            page = apiRequest.getPage(name)
            military_conflict_infoboxes = wikilib.get_templates(page, ['infobox military conflict', 'infobox battle', 'infobox militärischer konflikt', 'infobox war', 'warbox', 'infobox siege', 'infobox military conflict timeline', 'multiwarbox'])

            # military_conflict_infoboxes = list(filter(lambda txt: wiki_template.parse_template(txt)['template_name'].lower() in ['infobox military conflict', 'infobox battle', 'infobox militärischer konflikt', 'infobox war', 'warbox'], infoboxes))
            if len(military_conflict_infoboxes) != 1:
                # print('Article `' + name + '` (' + wikidata_id + ') has ' + str(len(military_conflict_infoboxes)) + ' military conflict infoboxes: ', [wiki_template.parse_template(one_of_infoboxes)['template_name']  for one_of_infoboxes in military_conflict_infoboxes])
                # print('not-one', wikidata_id, name, sep='\t')
                continue
            infobox = military_conflict_infoboxes[0]

            # infoboxes = wikilib.get_infoboxes(page)
            # if infoboxes[0] != military_conflict_infoboxes[0]:
                # print('found different infobox earlier', wikidata_id, name, sep='\t')
                # continue
            # print('nothing-interesting', wikidata_id, name, sep='\t')

            # continue

            if print_detailed_info:
                print(infobox)
            parsed_template = wiki_template.parse_template(infobox)['options']
            date_infos = get_date_infos_for_infobox(parsed_template)

            status = date_infos['status']
            if wikidata_has_claims:
                status += '/wikidata-has-statements'

            if status not in ['?', '+']:
                continue
                status = 'skip'
            if name in already_processed:
                status = 'skip'

            start_time, end_time, point_in_time = None, None, None
            if len(date_infos['dates']) == 2:
                start_time, end_time = date_infos['dates']
                start_time = dateToWikidataFormat(*start_time, withPrecision=True)
                end_time = dateToWikidataFormat(*end_time, withPrecision=True)
            elif len(date_infos['dates']) == 1:
                point_in_time = date_infos['dates'][0]
                point_in_time = dateToWikidataFormat(*point_in_time, withPrecision=True)

            csv_writer.writerow([idx, status, name, wikidata_id, date_infos['date_raw'], date_infos['date_formatted'], point_in_time, start_time, end_time])
        except Exception as e:
            continue
            status = 'parsing-error'
            status = 'skip'
            if date_infos and ('date_raw' in date_infos):
                csv_writer.writerow([idx, status, name, wikidata_id, date_infos['date_raw'], '', '','',''])
            else:
                csv_writer.writerow([idx, status, name, wikidata_id, '', '',  '','',''])
            print('Error with `' + name + '` page:',  file=sys.stderr)
            # print(e, file=sys.stderr)
            # traceback.print_exc(file=sys.stderr)

if __name__ == '__main__':
    main()
