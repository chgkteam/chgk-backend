import unittest
import parsing_dates

class TestDateParsing(unittest.TestCase):
    def test_parse_date_one_side(self):
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 ad'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 a.d.'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 b.c.'), [12, 1, 3456, 'bc'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 bce'), [12, 1, 3456, 'bc'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 c.e.'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456 ce'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('january 12, 3456'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january 12, 345'), [12, 1, 345, None])
        self.assertEqual(parsing_dates.parse_date('january 12, 34'), [12, 1, 34, None])
        self.assertEqual(parsing_dates.parse_date('january 12, 24'), [12, 1, 24, None])
        self.assertEqual(parsing_dates.parse_date('january 12, 2'), [12, 1, 2, None])
        self.assertEqual(parsing_dates.parse_date('january 12 3456'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january 1st, 3456'), [1, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january 2nd, 3456'), [2, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january 3rd, 3456'), [3, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january 12-th, 3456'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('12 january, 3456'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('12 january, 3456 ad'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('12 january 3456 ad'), [12, 1, 3456, 'ad'])
        self.assertEqual(parsing_dates.parse_date('3456, 12 january'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('3456 january 12'), [12, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('12 january'), [12, 1, None, None])
        self.assertEqual(parsing_dates.parse_date('january 12'), [12, 1, None, None])
        self.assertEqual(parsing_dates.parse_date('12'), [12, None, None, None])
        self.assertEqual(parsing_dates.parse_date('january 3456'), [None, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('january, 3456'), [None, 1, 3456, None])
        self.assertEqual(parsing_dates.parse_date('3456 january b.c.'), [None, 1, 3456, 'bc'])
        self.assertEqual(parsing_dates.parse_date('january'), [None, 1, None, None])
        self.assertEqual(parsing_dates.parse_date('3456'), [None, None, 3456, None])
        self.assertEqual(parsing_dates.parse_date('3456 bc'), [None, None, 3456, 'bc'])
        self.assertEqual(parsing_dates.parse_date('345'), [None, None, 345, None])
        self.assertEqual(parsing_dates.parse_date('24 bc'), [None, None, 24, 'bc'])

        self.assertEqual(parsing_dates.parse_date('{{Start date|345}}'), [None, None, 345, None])
        self.assertEqual(parsing_dates.parse_date('{{Start date|345|6}}'), [None, 6, 345, None])
        self.assertEqual(parsing_dates.parse_date(r'{{Start date|345|06}}'), [None, 6, 345, None])
        self.assertEqual(parsing_dates.parse_date(r'{{Start date|345|06|7}}'), [7, 6, 345, None])
        self.assertEqual(parsing_dates.parse_date(r'{{Start date|345|09|7}}'), [7, 9, 345, None])
        self.assertEqual(parsing_dates.parse_date(r'{{Start date|345|06|7|df=y}}'), [7, 6, 345, None])
        self.assertEqual(parsing_dates.parse_date(r'{{Start date|345|06|7|df=yes}}'), [7, 6, 345, None])

        self.assertEqual(parsing_dates.parse_date('15, 1724'), [15, None, 1724, None]) # in "January 14-15, 1724"
        self.assertEqual(parsing_dates.parse_date('15, 24 bc'), [15, None, 24, 'bc'])
        self.assertEqual(parsing_dates.parse_date('15, 24'), [15, None, 24, None])

        self.assertEqual(parsing_dates.parse_date('abcd'), [None, None, None, None])

    def test_parse_date_range(self):
        self.assertEqual(parsing_dates.parse_date_range('january 12, 3456 ad - march 15, 3462 ad'), [[12, 1, 3456, 'ad'], [15, 3, 3462, 'ad']])
        self.assertEqual(parsing_dates.parse_date_range('january 12 - march 15, 2042 ad'), [[12, 1, 2042, 'ad'], [15, 3, 2042, 'ad']])
        self.assertEqual(parsing_dates.parse_date_range('january 12, 2042 bc - march 15, 1234'), [[12, 1, 2042, 'bc'], [15, 3, 1234, None]])
        self.assertEqual(parsing_dates.parse_date_range(r'{{Start date|1862|3|23}} - {{End date|1864|4|26}}'), [[23, 3, 1862, None], [26, 4, 1864, None]])
        self.assertEqual(parsing_dates.parse_date_range('january 12 - march 15, 2042'), [[12, 1, 2042, None], [15, 3, 2042, None]])
        self.assertEqual(parsing_dates.parse_date_range('january 12 - 15, 2042'), [[12, 1, 2042, None], [15, 1, 2042, None]])
        self.assertEqual(parsing_dates.parse_date_range('january 12-15, 2042<br>(2 days)'), [[12, 1, 2042, None], [15, 1, 2042, None]])
        self.assertEqual(parsing_dates.parse_date_range('2042, january 12 - 15'), [[12, 1, 2042, None], [15, 1, 2042, None]])
        self.assertEqual(parsing_dates.parse_date_range('2000, january 12 - 15'), [[12, 1, 2000, None], [15, 1, 2000, None]])
        self.assertEqual(parsing_dates.parse_date_range('january 12 - 15, 2042 ad'), [[12, 1, 2042, 'ad'], [15, 1, 2042, 'ad']])
        self.assertEqual(parsing_dates.parse_date_range('january 12 - march, 2042 ad'), [[12, 1, 2042, 'ad'], [None, 3, 2042, 'ad']])
        self.assertEqual(parsing_dates.parse_date_range('october 1945 - 15 march 1976'), [[None, 10, 1945, None], [15, 3, 1976, None]])
        self.assertEqual(parsing_dates.parse_date_range('1945 - 15 march 1976'), [[None, None, 1945, None], [15, 3, 1976, None]])
        self.assertEqual(parsing_dates.parse_date_range('1945 - march 1976'), [[None, None, 1945, None], [None, 3, 1976, None]])
        self.assertEqual(parsing_dates.parse_date_range('march 1945 - 1976'), [[None, 3, 1945, None], [None, None, 1976, None]])
        self.assertEqual(parsing_dates.parse_date_range('2013, march 13 - 15'), [[13, 3, 2013, None], [15, 3, 2013, None]])
        self.assertEqual(parsing_dates.parse_date_range('1914 - 17'), [[None, None, 1914, None], [None, None, 1917, None]])
        self.assertEqual(parsing_dates.parse_date_range('1941 - 45'), [[None, None, 1941, None], [None, None, 1945, None]])
        self.assertEqual(parsing_dates.parse_date_range('1799 - 836'), [[None, None, 1799, None], [None, None, 1836, None]])
        self.assertEqual(parsing_dates.parse_date_range('2000 bc - 1976 bc'), [[None, None, 2000, 'bc'], [None, None, 1976, 'bc']])
        self.assertEqual(parsing_dates.parse_date_range('1976 bc - 2000 bc'), [[None, None, 2000, 'bc'], [None, None, 1976, 'bc']])
        self.assertEqual(parsing_dates.parse_date_range('941 - 45'), [[None, None, 941, None], [None, None, 945, None]])
        self.assertEqual(parsing_dates.parse_date_range('129 - 119 bc'), [[None, None, 129, 'bc'], [None, None, 119, 'bc']])
        self.assertEqual(parsing_dates.parse_date_range('19 - 29'), [[None, None, 19, None], [None, None, 29, None]])
        self.assertEqual(parsing_dates.parse_date_range('1923 - april, 1924'), [[None, None, 1923, None], [None, 4, 1924, None]])
        self.assertEqual(parsing_dates.parse_date_range('19 - april, 29'), [[None, None, 19, None], [None, 4, 29, None]])
        self.assertEqual(parsing_dates.parse_date_range('3 - 5'), [[None, None, 3, None], [None, None, 5, None]])
        self.assertEqual(parsing_dates.parse_date_range('29 - 19 bc'), [[None, None, 29, 'bc'], [None, None, 19, 'bc']])
        self.assertEqual(parsing_dates.parse_date_range('5 - 3 bc'), [[None, None, 5, 'bc'], [None, None, 3, 'bc']])
        self.assertEqual(parsing_dates.parse_date_range('941 bc - 45'), [[None, None, 941, 'bc'], [None, None, 45, None]])
        self.assertEqual(parsing_dates.parse_date_range('41 bc - 945 ad'), [[None, None, 41, 'bc'], [None, None, 945, 'ad']])
        self.assertEqual(parsing_dates.parse_date_range('41 bc - 945'), [[None, None, 41, 'bc'], [None, None, 945, None]])
        self.assertEqual(parsing_dates.parse_date_range('41 - 945 bc'), [[None, None, 945, 'bc'], [None, None, 41, 'bc']])

    def test_extend_date(self):
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, None], [15, 3, 2042, None]), ([12, 1, 2042, 11], [15, 3, 2042, 11]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, 'bc'], [15, 3, 2042, None]), ([12, 1, -2042, 11], [15, 3, 2042, 11]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, 'bc'], [15, 3, 2042, 'bc']), ([12, 1, -2042, 11], [15, 3, -2042, 11]))
        self.assertEqual(parsing_dates.extend_date([None, 2, 30, 'bc'], [None, 5, 20, 'ad']), ([1, 2, -30, 10], [31, 5, 20, 10]))
        self.assertEqual(parsing_dates.extend_date([None, None, 1945, None], [None, 3, 1976, None]), ([1, 1, 1945, 9], [31, 3, 1976, 10]))
        self.assertEqual(parsing_dates.extend_date([None, None, 2000, 'bc'], [None, None, 1976, 'bc']), ([1, 1, -2000, 9], [31, 12, -1976, 9]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, 'ad'], [None, 3, 2042, 'ad']), ([12, 1, 2042, 11], [31, 3, 2042, 10]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, 'ad'], [None, 4, 2042, 'ad']), ([12, 1, 2042, 11], [30, 4, 2042, 10]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2042, 'ad'], [None, 2, 2042, 'ad']), ([12, 1, 2042, 11], [28, 2, 2042, 10]))
        self.assertEqual(parsing_dates.extend_date([12, 1, 2044, 'ad'], [None, 2, 2044, 'ad']), ([12, 1, 2044, 11], [29, 2, 2044, 10])) # leap year
        self.assertEqual(parsing_dates.extend_date([12, 1, 1900, 'ad'], [None, 2, 1900, 'ad']), ([12, 1, 1900, 11], [28, 2, 1900, 10])) # non-leap year
        self.assertEqual(parsing_dates.extend_date([12, 1, 2000, 'ad'], [None, 2, 2000, 'ad']), ([12, 1, 2000, 11], [29, 2, 2000, 10])) # non-leap year
        self.assertEqual(parsing_dates.extend_date([None, 3, 1945, None], [None, None, 1976, None]), ([1, 3, 1945, 10], [31, 12, 1976, 9]))
        # Косяк с шаблоном "4 and 5 May 1800"
        # Косяк с шаблоном "28 [[Common Era|CE]]"

    # добавить тесты на вики-разметку
    # self.assertEqual(parsing_dates.<...>('18/19 May 1812'), [[18, 5, 1812, None], [19, 5, 1812, None]])
    # self.assertEqual(parsing_dates.<...>('18\19 May 1812'), [[18, 5, 1812, None], [19, 5, 1812, None]])
    # September/October 1792
    # 10–-28 May 1941
    # [[circa]] September, 9 [[Common Era|C.E.]]
if __name__ == '__main__':
    unittest.main()
