import os, sys, traceback
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import wikilib
import parsing_dates
import apiRequest
import wikidata_dates
import parseToDB
import sys

infoboxType = sys.argv[1]

with sys.stdout as fw:
    for line in sys.stdin:
        try:
            title = line.strip()

            page = apiRequest.getPage(title)
            raw_infobox = wikilib.get_infobox(page)
            infoboxData = parseToDB.infobox_parser(infoboxType)(raw_infobox)
            if 'date' not in infoboxData:
                print('Missing `date` field in infobox for article', title, file = fw)
                continue
            parsed_date = infoboxData['date']
            wikidata_date = wikidata_dates.getWikidataDate(apiRequest.wikidataPropertiesByTitle(title))

            message = ''
            parser_failed  =  (parsed_date == ([None, None, None], [None, None, None]))
            wikidata_missing  =  (wikidata_date is None) or (wikidata_date == ([None, None, None], [None, None, None]))
            if parser_failed and wikidata_missing:
                message = 'Error - No data in wikidata; Parser failed'
            elif wikidata_missing:
                message = 'Error - No data in wikidata; Parsed successfully'
            elif parser_failed:
                message = 'Error - Wikidata has data; Parsed pailed'
            elif parsed_date != wikidata_date:
                message = 'Error - Parsed date does not coincide with date from Wikidata'
            elif parsed_date == wikidata_date:
                message = 'Ok'

            # print('Infobox:', file = fw)
            # for (key, value) in infoboxData.items():
            #     print("\t{}:\t{}".format(key, value), file = fw)
            print(raw_infobox, file = fw)
            print('Title:', title, file = fw)
            print('\tResult:\t{}'.format(message), file = fw)
            print("\t{}:\t{}".format('raw date', infoboxData['date_raw']), file = fw)
            print("\t{}:\t{}".format('parsed date', parsed_date), file = fw)
            print("\t{}:\t{}".format('wikidata date', wikidata_date), file = fw)
            print('-------------------------------------', file = fw)
        except Exception as e:
            traceback.print_exc(file=fw)
